package com.example.partdtasktwo;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

public class ListFragment extends Fragment implements OnItemClickListener {

    private RecyclerView recyclerView;
    private ContactAdapter contactAdapter;

    private Button addContact;
    private LinearLayout fieldAddContact;
    private EditText addName;
    private EditText addEmail;
    private EditText addAddress;
    private EditText addPhone;
    private Button add;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_list, container, false);
        recyclerView = root.findViewById(R.id.list);
        recyclerView.setLayoutManager(new LinearLayoutManager(root.getContext()));
        contactAdapter = new ContactAdapter(root.getContext(), Generator.generate(), this);
        recyclerView.setAdapter(contactAdapter);

        fieldAddContact = root.findViewById(R.id.field_add_contact);
        addContact = root.findViewById(R.id.add_contact);
        addContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fieldAddContact.setVisibility(View.VISIBLE);
            }
        });

        addName = root.findViewById(R.id.add_name);
        addEmail = root.findViewById(R.id.add_email);
        addAddress = root.findViewById(R.id.add_address);
        addPhone = root.findViewById(R.id.add_phone);
        add = root.findViewById(R.id.add);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!addName.getText().toString().isEmpty() &&
                        !addEmail.getText().toString().isEmpty() &&
                        !addAddress.getText().toString().isEmpty() &&
                        !addPhone.getText().toString().isEmpty()) {
                    String name = addName.getText().toString();
                    String email = addEmail.getText().toString();
                    String address = addAddress.getText().toString();
                    int phone = Integer.parseInt(addPhone.getText().toString());
                    contactAdapter.addContact(name, email, address, phone);
                    addName.getEditableText().clear();
                    addEmail.getEditableText().clear();
                    addAddress.getEditableText().clear();
                    addPhone.getEditableText().clear();
                    fieldAddContact.setVisibility(View.GONE);
                    contactAdapter.notifyDataSetChanged();
                } else {
                    new AlertDialog.Builder(getActivity())
                            .setTitle("Ошибка")
                            .setMessage("Заполните все поля!!")
                            .setPositiveButton("Ок", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            }).show();
                }
            }
        });
        return root;
    }

    @Override
    public void onItemClick(Contact contact) {
        Intent intent = new Intent(getActivity(), InfoActivity.class);
        intent.putExtra("id", contact.getIdPhoto());
        intent.putExtra("name", contact.getName());
        intent.putExtra("email", contact.getEmail());
        intent.putExtra("address", contact.getAddress());
        intent.putExtra("phone", contact.getPhone());
        startActivity(intent);
    }
}
