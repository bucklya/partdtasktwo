package com.example.partdtasktwo;

import android.widget.ImageView;

import java.util.HashMap;

public class Contact {
    private String name;
    private String email;
    private String address;
    private int phone;
    private int idPhoto;

    public Contact(String name, String email, String address, int phone, int idPhoto) {
        this.name = name;
        this.email = email;
        this.address = address;
        this.phone = phone;
        this.idPhoto = idPhoto;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getAddress() {
        return address;
    }

    public int getPhone() {
        return phone;
    }

    public int getIdPhoto() {
        return idPhoto;
    }
}
