package com.example.partdtasktwo;

import java.util.ArrayList;
import java.util.List;

public final class Generator {

    private Generator() {
    }

    public static List<Contact> generate() {
        List<Contact> list = new ArrayList<>();
        list.add(new Contact("Иван Кузнецов", "ivankuz@gmail.com", "ул. Плехановская 35", 3956433, R.drawable.photo1));
        list.add(new Contact("Вероника Соловей", "soloveychik@gmail.com", "ул. Артема 54", 3925464, R.drawable.photo2));
        return list;
    }
}
