package com.example.partdtasktwo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class InfoActivity extends AppCompatActivity {

    ImageView photo;
    TextView name;
    TextView email;
    TextView address;
    TextView phone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);

        photo = findViewById(R.id.info_photo);
        name = findViewById(R.id.info_name);
        email = findViewById(R.id.info_email);
        address = findViewById(R.id.info_address);
        phone = findViewById(R.id.info_phone);

        Bundle arguments = getIntent().getExtras();

        photo.setImageResource(arguments.getInt("id"));
        name.setText(arguments.get("name").toString());
        email.setText(arguments.get("email").toString());
        address.setText(arguments.get("address").toString());
        phone.setText(arguments.get("phone").toString());
    }
}
