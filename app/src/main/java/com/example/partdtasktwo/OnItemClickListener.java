package com.example.partdtasktwo;

public interface OnItemClickListener {
    void onItemClick (Contact contact);
}
